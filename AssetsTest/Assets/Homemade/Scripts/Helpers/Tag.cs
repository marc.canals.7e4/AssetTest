﻿public static class Tag
{
    public const string Coin = "Coin";
    public const string Fire = "Fire";
    public const string Interactable = "Interactable";
    public const string Player = "Player";
    public const string PlayerUI = "PlayerUI";
}
