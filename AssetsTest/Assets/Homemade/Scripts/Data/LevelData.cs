﻿using UnityEngine;

[CreateAssetMenu(fileName = "levelData", menuName = "Data/levelData", order = 1)]
public class LevelData : ScriptableObject
{
    public int totalCoins;
    public GameObject[] fireworks;

    private void OnEnable() => SetLevelData();

    public void SetLevelData()
    {
        totalCoins = GameObject.FindGameObjectsWithTag(Tag.Coin).Length;
        fireworks = GameObject.FindGameObjectsWithTag(Tag.Fire);
    }
}