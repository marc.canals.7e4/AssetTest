﻿using System;
using UnityEngine;

[CreateAssetMenu(fileName = "playerData", menuName = "Data/playerData", order = 1)]
public class PlayerData : ScriptableObject
{
    public int currentCoins;

    private void OnEnable()
    {
        Reset();
    }

    public void Reset() => currentCoins = 0;
}