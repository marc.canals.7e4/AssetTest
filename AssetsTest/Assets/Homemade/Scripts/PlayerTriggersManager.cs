﻿using UnityEngine;

[RequireComponent(typeof(CapsuleCollider))]
public class PlayerTriggersManager : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        switch (other.tag)
        {
            case Tag.Coin:
                other.GetComponent<IInteractable>().Interact();
                break;
            default:
                return;
        }
    }
}