﻿using System;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI coinsText;
    [SerializeField] private Button resetButton;

    private void Awake()
    {
        resetButton.gameObject.SetActive(false);
    }

    private void Start()
    {
        GameManager.Instance.UpdateUI += UpdateText;
        GameManager.Instance.FinishedUI += ShowResetButton;
    }

    private void UpdateText() => coinsText.text =
        "Coins: " + GameManager.Instance.playerData.currentCoins + " / " + GameManager.Instance.levelData.totalCoins;

    private void ShowResetButton() => resetButton.gameObject.SetActive(true);

    public void ResetScene() => SceneManager.LoadScene(sceneBuildIndex: 0);

    private void OnDestroy()
    {
        GameManager.Instance.UpdateUI -= UpdateText;
        GameManager.Instance.FinishedUI -= ShowResetButton;
    }
}