﻿using UnityEngine;

public class GameManager : MonoBehaviour
{
    public PlayerData playerData;
    public LevelData levelData;

    public delegate void UIHandler();

    public event UIHandler UpdateUI;
    public event UIHandler FinishedUI;

    public static GameManager Instance { get; private set; }

    private void Awake()
    {
        if (Instance != null && Instance != this) Destroy(gameObject);
        Instance = this;
    }

    private void Start()
    {
        if (playerData.currentCoins > 0)
            playerData.Reset();
        levelData.SetLevelData();
        SetUI();
    }


    public void SetUI() => UpdateUI?.Invoke();

    public void FinishedGameChecker()
    {
        if (levelData.totalCoins > playerData.currentCoins) return;

        foreach (var t in levelData.fireworks)
            t.GetComponent<ParticleSystem>().Play();

        FinishedUI?.Invoke();
    }
}