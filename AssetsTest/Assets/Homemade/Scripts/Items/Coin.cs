﻿using UnityEngine;

public class Coin : MonoBehaviour, IInteractable
{
    public void Interact()
    {
        GameManager.Instance.playerData.currentCoins++;
        GameManager.Instance.SetUI();
        GameManager.Instance.FinishedGameChecker();
        Destroy(gameObject);
    }
}